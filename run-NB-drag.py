#!/usr/bin/env python

import os
import sys
import math
import random
import pathlib
import argparse
from parse import parse
import numpy as np
import pandas as pd

from hybrid import Constants, SwarmTools, DiskTools
from hybrid import disk_parameters as diskparam
from hybrid import Powerlaws as plaw
from hybrid.Utility import Args, Param, Params
from hybrid.Simulation import *
from hybrid.REBOUNDSimulation import *
from hybrid.dragforce import *
from hybrid.Timescales import *


params = Params([
    Param(name='softening',     short='s', default=   0.0,    unit='rHill',     help='N-body softening length to use for active bodies (in rHill units)'),
    Param(name='duration',      short='T', default=20000,    unit='yr',        help='how long to run the simulation (years)'),
    Param(name='num-swarms',    short='N', default=   800,                      help='how many swarm particles to use'),
    Param(name='num-embryos',   short='NE',default=     2,                      help='How many embryo AND planets are in the system'),
    Param(name='num-snapshots',            default=   100,                      help='how many snapshots to take'),
    Param(name='swarm-ring',               default=    66,    unit='au',        help='set up the ring of swarms, middle of the ring'),
    Param(name='ring-thickness',           default=     6,    unit='au',        help='radial extent of the swarm ring'),
    Param(name='rho-internal',             default=   0.05,    unit='g/cm³',     help='internal density of solid body'),
    Param(name='dv0',                      default=  0.001,    unit='vKepler',   help='initial turbulent velocity defined as mean eccentricity'),
    Param(name='swarm-mass',               default= 3.e+7,    unit='g',         help=' mass of solid material in a swarm'),
    Param(name='embryo-mass',              default=     1,                      help='embryo mass in units of moon mass'),
    Param(name='mass-center',              default=   2.5,    unit='Msun',      help='mass of the central object'),
    Param(name='restitution',   short='r', default=   0.6,                      help='restitution coefficient for hard sphere collisions')
])
paramsInFilename = set(['duration', 'num-swarms'])

parser = argparse.ArgumentParser(description='Run viscous stirring test with N-body simulation.')
parser.add_argument('--run',              action='store_true',                  help='run the simulation')
parser.add_argument('--write',            action='store_true',                  help='write results to .tsv file')
parser.add_argument('--incremental',      action='store_true',                  help='write results incrementally')
parser.add_argument('--log',              action='store_true',                  help='produce log messages')
parser.add_argument('--plot',             action='store_true',                  help='make plots for the current run or the given data files')
parser.add_argument('--outdir',           default='data',             type=str, help='output directory for data file')
parser.add_argument('--files',            action='extend', nargs='+', type=str, help='data files to plot')
params.add_to_argparser(parser)
if len(sys.argv) == 1:
    parser.print_usage()
    sys.exit(1)
#args = parser.parse_args()
args = params.init_default_args(parser.parse_args())

random.seed(42)
np.random.seed(42)

# set the time of simulation, duration is the number of orbits of inner planet
Nplanet = diskparam.Nplanet
aplanet  = diskparam.ajupiter_AU
aring = diskparam.Rad0 
Period_planet = 2 * math.pi * aplanet *math.sqrt(aplanet/args.mass_center)
Period_Ring = 2 * math.pi * aplanet *math.sqrt(aring/args.mass_center)
T      = args.duration  # (years)

# setup if restitution 
eps0    = args.restitution

# setup Swarms
MCenter = diskparam.Mcenter   # args.mass_center
rSwarm  = diskparam.Rad0      # args.swarm_ring 
drSwarm = diskparam.dR_ring   # args.ring_thickness
NSwarm  = args.num_swarms
MSwarm  = args.swarm_mass / Constants.Msun_g 
ρmatter = args.rho_internal / Constants.Msun_g * Constants.AU_cm**3   # alternatively, can use Constants.rho_internal
dSwarm_cgs = (3./4. * args.swarm_mass  / (math.pi * args.rho_internal ))**(1./3.) 
dSwarm  = dSwarm_cgs / Constants.AU_cm
print ('The tracers are: M/M_moon, Radius, density:  ', args.swarm_mass/Constants.Mmoon_g, dSwarm*Constants.AU_cm, args.rho_internal  )


#setup Embryos and Planets
NEmbryo = args.num_embryos
MEmbryo = args.embryo_mass * Constants.Mmoon        # in units of Msun
MEmbryo_g = args.embryo_mass * Constants.Mmoon_g 
dEmbryo = Constants.Rmoon_AU   # (3/4 * MEmbryo / (math.pi * ρmatter))**(1/3)
dEmbryo_cgs = Constants.Rmoon_cm
aEmbryo = diskparam.amoon_AU 
rEHill  = aEmbryo * (MEmbryo / (3*MCenter))**(1/3)


if args.log:
    tau_orb, tau_Gs, tau_GI, tau_coll, StokesN = timescales(args.dv0, NSwarm, args.swarm_mass, dSwarm_cgs)  

stem = 'run-NB-dragring2b'
datafile = '{}/{} {}.tsv'.format(args.outdir, stem, params.serialize_string(args, subset=paramsInFilename))


if args.write:
    pathlib.Path(args.outdir).mkdir(exist_ok=True)
    params.to_file(args, datafile)

if args.run:
    Gfac = 1./0.01720209895 * math.sqrt(Constants.G)  # Gaussian constant  # use if Sun and planet data are form NASA archiv
    nbSim = REBOUNDSimulation(
        sun=Args(m=MCenter*1.00000597682 ),
        planets=[
            #Args(m=Constants.Mjupiter , a=diskparam.ajupiter_AU, r=Constants.Rjupiter_AU, e=0.000, inc=0.0000),
            Args(m=MEmbryo/1000., a=diskparam.ajupiter_AU, r=Constants.Rmoon_AU/10., e=0.000, inc=0.0000),
            Args(m=MEmbryo/1000., a=diskparam.Rad0, r=Constants.Rmoon_AU/10., e=0., inc=0., M=3.14),
        ],
        tracers=[
            SwarmTools.generate_uniform_tracer(
               mTracer = MSwarm,
               aMin = diskparam.amoon_AU - 6.,
               aMax = diskparam.amoon_AU + 6., 
               dsize = dSwarm,                     # in AU
               eAvg = 0.0, #args.dv0,
               incAvg = 0.0 # 0.5*args.dv0 
               )
            for _ in range(NSwarm)])
    nbSim.sim.N_active=1  # +NEmbryo
    nbSim.sim.testparticle_type=1
    
    ps=nbSim.sim.particles
    nbSim.sim.additional_forces = my_dragforce(nbSim.sim,dSwarm_cgs)
    nbSim.sim.force_is_velocity_dependent = 1

    # --- USE if collisions are occuring: -----
    #nbSim.sim.coefficient_of_restitution = plaw.cor_bridges  #plaw.eps_restitution(eps0), '' TODO: why it only works with cor_bridges ?
    #nbSim.sim.softening  = args.softening*rHill
    #nbSim.sim.exit_min_distance=dSwarm
    #nbSim.sim.collision  = "direct"
    #nbSim.sim.collision_resolve = "hardsphere" 
    #
    # --- Integrators choice and how to set them up: -----
    #
    #nbSim.sim.integrator = 'MERCURIUS'
    #nbSim.sim.dt         = Period_planet * diskparam.timestep_fix
    #
    #or
    #
    nbSim.sim.integrator = 'whfast'
    nbSim.sim.ri_whfast.corrector = 11
    nbSim.sim.dt         = Period_Ring * diskparam.timestep_fix
    #
    #or 
    #
    #nbSim.sim.integrator = 'ias15'
    #nbSim.sim.ri_ias15.min_dt = 1.e-6


    # Setup calculation time row: ----
    times = np.linspace(start=0, stop=T * Period_Ring, num=args.num_snapshots)  # years/(2π)

    # Prepare Dataframe for outputs to store the list of particles for aech timestep
    plotDataRows = len(times) * nbSim.N
    plotData = pd.DataFrame.from_dict({
        'id':  np.zeros(shape=[plotDataRows], dtype=np.int64),
        't':   np.zeros(shape=[plotDataRows], dtype=np.float),
        'x':   np.zeros(shape=[plotDataRows], dtype=np.float),
        'y':   np.zeros(shape=[plotDataRows], dtype=np.float),
        'z':   np.zeros(shape=[plotDataRows], dtype=np.float),
        'a':   np.zeros(shape=[plotDataRows], dtype=np.float),
        'e':   np.zeros(shape=[plotDataRows], dtype=np.float),
        'inc': np.zeros(shape=[plotDataRows], dtype=np.float),
        'vx':  np.zeros(shape=[plotDataRows], dtype=np.float),
        'vy':  np.zeros(shape=[plotDataRows], dtype=np.float),
        'vz':  np.zeros(shape=[plotDataRows], dtype=np.float),
        'm':  np.zeros(shape=[plotDataRows], dtype=np.float)
    })

    def timeRowsInclusive(i):
        return slice(i * nbSim.N, (i + 1) * nbSim.N - 1)  # caveat: .loc[] slicing is *inclusive*!
    for i, t in enumerate(times):
        rows = timeRowsInclusive(i)
        plotData.loc[rows, 'id'] = range(nbSim.N)
        plotData.loc[rows, 't'] = t
    def captureNBState(i, t, nbState):
        if args.log:
            print('t={:.2f} years'.format(t/(2*math.pi)))
        rows = timeRowsInclusive(i)
        plotData.loc[rows, 'x'] = nbState.x.array
        plotData.loc[rows, 'y'] = nbState.y.array
        plotData.loc[rows, 'z'] = nbState.z.array
        plotData.loc[rows, 'a'] = nbState.a.array
        plotData.loc[rows, 'e'] = nbState.e.array
        plotData.loc[rows, 'inc'] = nbState.inc.array
        plotData.loc[rows, 'vx'] = nbState.vx.array
        plotData.loc[rows, 'vy'] = nbState.vy.array
        plotData.loc[rows, 'vz'] = nbState.vz.array
        plotData.loc[rows, 'm'] = nbState.m.array

        if args.write and args.incremental:
            plotData.loc[rows].to_csv(datafile, sep='\t', index=False, header=i == 0, mode='a')

    # Run here a time integration using nb or hybrid
    run_nb_simulation(
        nbSim=nbSim,
        times=times,
        captureNBState=captureNBState)



if args.write and not args.incremental:
    plotData.to_csv(datafile, mode='a', sep='\t', index=False)


def plot(ldatafile, outdir, lplotdata, largs, lnembryo,litau_s):
    import matplotlib.pyplot as plt


    lplanet = 1
    NP = lplanet
    NE = lnembryo - lplanet
    NS = np.max(lplotdata.id) - lnembryo 
    rootMeanSquare_ns = lambda x: np.sqrt(np.sum(x**2)/NS)  # do not forget averaging
    MeanSquare_ns     = lambda z: np.sum(z**2)/NS
    rootMeanSquare_ne = lambda x: np.sqrt(np.sum(x**2)/NE)  # do not forget averaging
    MeanSquare_ne     = lambda z: np.sum(z**2)/NE
    Mean_ns           = lambda x: np.sum(x)/NS
    Mean_ne           = lambda x: np.sum(x)/NE
    Mean_np           = lambda x: np.sum(x)/NP
    def rootMeanSquare(x):
        return np.sqrt(np.sum(x**2)/len(x))
    def rootMeanSquareErr(x):
        mean = rootMeanSquare(x)
        return np.sqrt(np.sum((x - mean)**2)/len(x))
    Square           = lambda y: (y*y)

    lplotdata['rho']= np.sqrt( lplotdata.x**2  + lplotdata.y**2 )
    lplotdata['vr'] = (lplotdata.x*lplotdata.vx  + lplotdata.y*lplotdata.vy) / lplotdata.rho 
    lplotdata['vphi']  = (lplotdata.x*lplotdata.vy  - lplotdata.y*lplotdata.vx) / lplotdata.rho 
    lplotdata['vkepl'] = np.sqrt(diskparam.Mcenter*Constants.G/lplotdata.a) # TODO: replace v_kepler (local) with v_kepler (a)
    lplotdata['dvphi'] = lplotdata.vphi-lplotdata.vkepl
    lplotdata['aMoment'] = lplotdata.m*lplotdata.vphi*lplotdata.rho

    PlanetData = lplotdata[(lplotdata.id < lplanet+1) ]
    EmbryoData = lplotdata[(lplotdata.id >= lplanet+1) & (lplotdata.id < lnembryo+1) ]
    SwarmData  = lplotdata[(lplotdata.id >= lnembryo+1)] 

    plotDataRMS = SwarmData \
        .groupby(SwarmData.t, as_index=False) \
        .agg({
            'e': rootMeanSquare_ns,
            'inc': rootMeanSquare_ns,
            'a': rootMeanSquare_ns,
        })
    plotDataErr = SwarmData \
        .groupby(SwarmData.t, as_index=False) \
        .agg({
            'e': rootMeanSquareErr,
            'inc': rootMeanSquareErr,
            'a': rootMeanSquareErr,
        })


    plotDataRMS_E = EmbryoData \
        .groupby(EmbryoData.t, as_index=False) \
        .agg({
            'e': rootMeanSquare,
            'inc': rootMeanSquare,
            'a': rootMeanSquare,
        })
    plotDataRMS_P = PlanetData \
        .groupby(PlanetData.t, as_index=False) \
        .agg({
            'e': rootMeanSquare,
            'inc': rootMeanSquare,  
            'a': rootMeanSquare,
        })


     # WANTED:
     # id=0 is planet, id=1 is embrio -- for this setup
     #         !  number of embrios should be known if generateEbryos is used !
     # step 1. transfer vx,vy , vz to vr, vphi vz
     # step 2. substract vphi - v_kepl ( as sqrt(GM/a), or as sqrt(GM/r_loc) ? )
     # step 3. average over planetesimals (mean route square) , or over embryos
     # step 4. plot delta v = sqrt( mean(delta v_pl**2) + mean(delta v_e**2) )

    t_years = plotDataRMS.t / Period_Ring # (2*math.pi)
    inc2e = plotDataRMS.inc / plotDataRMS.e

    # step 1.
    
    def X2R(x,y):
        return  np.sqrt(x**2 + y**2)
    def C2CYL_r(x,ux,y,uy):
        return (x*ux + y*uy)/np.sqrt(x**2 + y**2)
    def C2CYL_phi(x,ux,y,uy):
        return (x*uy - y*ux)/np.sqrt(x**2 + y**2)
    def vkepl_loc(x,y):
        return np.sqrt(1/np.sqrt(x**2)+y**2)
 
    # step 3.  
    swarm_v2 = SwarmData \
         .groupby(SwarmData.t ) \
         .agg({
             'vz': MeanSquare_ns,
             'vr': MeanSquare_ns,
             'dvphi': MeanSquare_ns,
         }) \
         .reset_index()
    embryo_v2 = EmbryoData \
         .groupby(EmbryoData.t ) \
         .agg({
             'vz': MeanSquare_ne,
             'vr': MeanSquare_ne,
             'dvphi': MeanSquare_ne,
         })\
         .reset_index()

         
    plotDataDist = lplotdata.rho \
        .groupby(pd.cut(lplotdata.rho, bins=rho_grid, labels=False )) \
        .size()
    # TODO: bin are not working yet    
    # alternative, average over al times, put in bins
     

    # calculate approaching velocity between swarms and embryo
    #
    swarm_dv = np.sqrt(swarm_v2.vz+swarm_v2.vr+swarm_v2.dvphi )
    embryo_dv = np.sqrt(embryo_v2.vz+embryo_v2.vr+embryo_v2.dvphi    )
    appro_v = np.sqrt(swarm_v2.vz+swarm_v2.vr+swarm_v2.dvphi  +  embryo_v2.vz+embryo_v2.vr+embryo_v2.dvphi    ) 
    swarm_v2['dv'] = np.sqrt(swarm_v2.vz+swarm_v2.vr+swarm_v2.dvphi )
    embryo_v2['dv'] = np.sqrt(embryo_v2.vz+embryo_v2.vr+embryo_v2.dvphi    )
    embryo_v2['va'] = np.sqrt(swarm_v2.vz+swarm_v2.vr+swarm_v2.dvphi  +  embryo_v2.vz+embryo_v2.vr+embryo_v2.dvphi    ) 
    print ('Relative or aproaching velocity:', appro_v, 'Who contributes?', swarm_dv, embryo_dv)
    print ('time :', t_years)
    print ('Relative or aproaching velocity:', embryo_v2.va, 'Who contributes?', swarm_v2.dv, embryo_v2.dv)
  

    havePositions = 'x' in lplotdata.columns and 'y' in lplotdata.columns
    fig = plt.Figure(figsize=[24, 12 if havePositions else 6])
    fig.subplots_adjust(wspace=0.3)
    nplots = (2, 3) if havePositions else (1, 2)
    axLin1 = fig.add_subplot(*nplots, 1,
        ylabel='eccentricity',
        xlabel='time /T_ring')
    axLin2 = fig.add_subplot(*nplots, 2,
        ylabel='inclination',
        xlabel='time /T_ring')
    if havePositions:
        axLin3 = fig.add_subplot(*nplots, 3,
            xlabel='time /T_ring', ylabel='Semimajor axis a[AU] ')
    if havePositions:
        axLin4 = fig.add_subplot(*nplots, 4,
            xlabel='time /T_ring', ylabel='Change of semimajor axis, (a-a_0)/a_0 ')
    if havePositions:
        axScatter2 = fig.add_subplot(*nplots, 5,
            xlabel='x (AU)', ylabel='y (AU)', title='t_end')
    if havePositions:
        axLin5 = fig.add_subplot(*nplots, 6,
            xlabel='time /T_ring', ylabel='Delta v [cm/s]')
    
    testfunk =  0.025  *  np.exp(- 0.5 *t_years * litau_s ) 
     

    color = 'tab:red'
    axLin1.plot(t_years, plotDataRMS.e, ls='-', color='black')
    axLin1.fill_between(t_years, (plotDataRMS.e - plotDataErr.e), (plotDataRMS.e + plotDataErr.e), color=color, alpha=0.15)
    axLin1.plot(t_years, plotDataRMS_E.e, ls='-', color='red')
    # if no vertical test
    #axLin2.plot(t_years, plotDataRMS.inc, ls='-', color='black')
    #axLin2.plot(t_years, testfunk, ls=':', color='green')
    #axLin2.fill_between(t_years, (plotDataRMS.inc - plotDataErr.inc), (plotDataRMS.inc + plotDataErr.inc), color=color, alpha=0.15)
    #axLin2.plot(t_years, plotDataRMS_E.inc, ls='-', color='red')
    # if making a vertical test
    axLin2.plot(t_years, SwarmData.z[SwarmData.id==3], ls='-', color='black')
    axLin2.plot(t_years, testfunk, ls=':', color='green')
    if havePositions:
        #prepare the difference in semi-major axis

        #prepare start and end:
        SwarmStart = SwarmData[SwarmData.t == 0 ]
        SwarmEnd   = SwarmData[SwarmData.t == np.max(SwarmData.t ) ]
        EmbryoStart = EmbryoData[EmbryoData.t == 0 ]
        EmbryoEnd   = EmbryoData[EmbryoData.t == np.max(EmbryoData.t ) ]
        PlanetStart = PlanetData[PlanetData.t == 0 ]
        PlanetEnd   = PlanetData[PlanetData.t == np.max(PlanetData.t ) ]

          
        axScatter2.plot(SwarmEnd.x, SwarmEnd.y, ls='', marker=',', ms=2, color='blue')
        axScatter2.plot(EmbryoEnd.x, EmbryoEnd.y, ls='', marker='o', ms=2, color='red')
        axScatter2.plot(PlanetEnd.x, PlanetEnd.y, ls='', marker='o', ms=3, color='green')



    print('shapes:', t_years.shape, appro_v.shape)

    if havePositions:
       axLin3.plot(t_years, plotDataRMS.a, ls='-', color='black')
       axLin3.fill_between(t_years, (plotDataRMS.a - plotDataErr.a), (plotDataRMS.a + plotDataErr.a), color=color, alpha=0.15)
       axLin3.plot(t_years, plotDataRMS_E.a, ls='-', color='red')

       a_diff = (plotDataRMS.a - plotDataRMS.a[0])/plotDataRMS.a[0]
       a_difE = (plotDataRMS_E.a - plotDataRMS_E.a[0])/plotDataRMS_E.a[0]
       axLin4.plot(t_years[t_years != 0.], a_diff[a_diff != 0.])


    if havePositions:
       axLin5.plot(t_years[t_years != 0.], embryo_v2.va[t_years != 0.]/Constants.v_unit, ls='-', color='black')
       axLin5.plot(t_years[t_years != 0.], embryo_v2.dv[t_years != 0.]/Constants.v_unit, ls=':', color='red')
       axLin5.plot(t_years[t_years != 0.], swarm_v2.dv[t_years != 0.]/Constants.v_unit, ls=':', color='blue')


    _, filename = os.path.split(ldatafile)
    basename, _ = os.path.splitext(filename)
    plotfile = '{}/{}.pdf'.format(outdir, basename)
    fig.savefig(plotfile)

if args.plot:
    
    # for plotting purposes:
    n_grid = 1200  #40
    rho_grid  = np.linspace(start=diskparam.Rad0-diskparam.dR_ring, stop=diskparam.Rad0+diskparam.dR_ring, num=n_grid) # 
    delta_rho = (2*6)/n_grid

    # call a disk routine once more, for rho_grid as locations
    istoptime = np.zeros(n_grid)
    Stokes = np.zeros(n_grid)
    gasspeed = np.zeros(n_grid)
    for i in range(len(rho_grid)):
        radSwarm = rho_grid[i]
        zSwarm = 0.0
  
        itau_stop, v_gas, nstokes, rep_H = DiskTools.calculate_stoppingtime(
              mass       = args.swarm_mass,
              asolid     = dSwarm_cgs,
              radSwarm   = radSwarm,
              zSwarm     = zSwarm
              ) 
        istoptime[i]=itau_stop
        Stokes[i] =  nstokes
        gasspeed[i] = v_gas

    itau_s = itau_stop * Constants.T_s * Period_Ring
    # TODO : do we need it in this particular form?

    if args.run:
        plot(datafile, args.outdir, plotData, args, NEmbryo, itau_s )
    else:
        if args.files is None:
            raise Exception('no data files given')
        for file in args.files:
            lplotData = pd.read_csv(file, sep='\t', comment='#')
            largs = params.from_file(file)
            print('Plotting {}'.format(file))
            plot(file, args.outdir, lplotData, largs, NEmbryo,itau_s)
