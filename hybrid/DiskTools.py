
import math
import numpy as np
from hybrid import disk_parameters as diskparam
from hybrid import Constants

####def sound_speed(rref,radSwarm,q_factor, Temperature0):
    
def sound_speed(radSwarm):
    # in cgs units
    q_factor     = diskparam.q_factor
    Temperature0 = diskparam.Temperature0
    rref         = diskparam.rref

    sound        = np.sqrt( Constants.kB * Temperature0*(radSwarm/rref)**(-q_factor)/Constants.meanw/Constants.mp )

    return sound


###def gas_profile(rref, radSwarm, zSwarm, p_factor, Sigma0, Mcenter, sound ):

def gas_profile(radSwarm, zSwarm, sound ):
    # in cgs units
    p_factor     = diskparam.p_factor
    q_factor     = diskparam.q_factor
    Temperature0 = diskparam.Temperature0
    Sigma0       = diskparam.Sigma0
    rref         = diskparam.rref
    Mcenter      = diskparam.Mcenter

    r_spher = np.sqrt(radSwarm**2 + zSwarm**2 ) * Constants.AU_cm        # cm
    R_cyl   = radSwarm * Constants.AU_cm                                 # cm
    GMMsun  =  Constants.G_cgs *Mcenter * Constants.Msun_g                 # 
    Orbital = np.sqrt( GMMsun /(r_spher)**3 )
    Height  = sound/Orbital
    # MY TRY 
    Omega0  =  np.sqrt( GMMsun /(rref)**3 )
    sound0  =  np.sqrt(Constants.kB * Temperature0*(rref)**(-q_factor)/Constants.meanw/Constants.mp)
    floor   = diskparam.gapfloor
    a_jup   = diskparam.ajupiter_AU
    h_jup   = 0.35 * a_jup     # after Pinilla 2012

    # let us work with number density:
    Sigma0_n = Sigma0 / (Constants.meanw*Constants.mp)
    # 
    # MY TRY : why, if I call sound as a function/argument (to calculate Height), there has been BIG numerical error?
    # ..have to calculate here locally a sound speed ! carefull: it is not dependent on z! 
    #gas_unperturb   = Sigma0_n /( math.sqrt(2.*math.pi)*Height ) * (radSwarm/rref)**(-p_factor) *np.exp( GMMsun /sound**2 * ( 1./r_spher - 1./R_cyl )  )
    # no trap
    gas_unperturb   = Sigma0_n /( math.sqrt(2.*math.pi)*Omega0*sound0) * (radSwarm/rref)**(3./2.+q_factor/2. -p_factor) *np.exp( GMMsun /sound**2 * ( 1./r_spher - 1./R_cyl )  )
    # with trap
    gastrap_n   = gas_unperturb *(1. - np.exp( -(radSwarm - a_jup)**4/2./h_jup**4)*(1. - floor)  )

    gas_n = gas_unperturb * (1.-diskparam.put_gap) + gastrap_n * diskparam.put_gap

    return gas_n

#def calculate_stoppingtime( 
 #       mass,
 #       asolid,
 #       rref, 
 #       Temperature0,
 #       q_factor,
 #       Sigma0,
 #       p_factor,
# #       a_jup,
# #       floor,
  #      radSwarm,
  #      zSwarm,
 #       Mcenter  ):
##

def calculate_stoppingtime( 
        mass,
        asolid,
        radSwarm,
        zSwarm   ):

    """ Generate the gas disk

    Parameters to feed:
    -------
    mass :              mass of particle, code unit,calculate internal density of planetesimal later
    asolid :            size of planetesimal, code unit
    radSwarm :          cyllindrical radius, in AU
    zSwarm   :          vertical coordinate, in AU
 
    Values to calculate:
    -------
    mfp :                mean free path
    itau_s:              inverted Stopping time in Stokes regime
    itau_e:              inverted Stopping time in Epstein regime  
    itau_stop :           choose inverted stopping time within Stokes and Epstein regimes
    g_gas    :           density of gas, g/cm^3
    n_gas    :           number density of gas, 1/cm^3
    O_gas    :           angular velocity of gas 1/s
    v_gas    :           velocity of gas, sub-keplerian, in cm/s
    n_stokes :           stokes number of the planetesimal at the given position

    Comment: 
    -------
    Stokes regime is applicable for any solid bodies larger then 100 cm, but check for each disk please.
    tau_s is independent of particular number density of the gas, but sensitive to Temperature as 1/sqrt(T)
    """
    
    rref         = diskparam.rref
    Mcenter      = diskparam.Mcenter
    """ Some helpful shortcuts """
    # internal density
    #rho_solid = 3. * mass * Constants.Msun_g / (4. *math.pi * (asolid) **3 )
    rho_solid = 3. * mass  / (4. *math.pi * (asolid) **3 )
    r_spher = np.sqrt(radSwarm**2 + zSwarm**2 ) * Constants.AU_cm        # cm
    R_cyl   = radSwarm * Constants.AU_cm                                 # cm
    GMMsun  =  Constants.G_cgs * Mcenter * Constants.Msun_g                 # 
    Orbital = np.sqrt( GMMsun /(r_spher)**3 )  

    """ Setup gas disk """  
    "1.  Sound speed: "
    sound  = sound_speed(radSwarm=radSwarm) 
    
    Height = sound/Orbital
    report_H = ( zSwarm*Constants.AU_cm)/Height

    " 2. Gas density: "
    gas_n  = gas_profile(radSwarm=radSwarm, zSwarm=zSwarm, sound=sound)

    gas_g = gas_n * Constants.meanw * Constants.mp
    ### n_gas  = gas_g /(Constants.meanw * Constants.mp)

    "3. Gas velocity: "
  
    # make values for r-H/2:
    H1 = (Height /1.e6)      # some realy small distance # 
    H1_AU = H1/Constants.AU_cm   ##  free choice, just take it small enough
    sound_m =   sound_speed(radSwarm=(radSwarm-H1_AU/2.) ) 
    sound_p =   sound_speed(radSwarm=(radSwarm+H1_AU/2.)  )

    gasg_m =  gas_profile(radSwarm=radSwarm-H1_AU/2., zSwarm=zSwarm, sound=sound_m)
    gasg_p =  gas_profile(radSwarm=radSwarm+H1_AU/2., zSwarm=zSwarm, sound=sound_p)

    # let's work with number density of gas right away, 
    deltaP = (sound_p**2 - sound_m**2)/H1 + sound**2 * (np.log(gasg_p) - np.log(gasg_m))/H1


    v_phi = np.sqrt( GMMsun/r_spher +  r_spher * deltaP)
    O_gas = v_phi / r_spher     # sub-keplerian angular velocity

    v_gas = v_phi * Constants.v_unit

    " 4. Mean free path: "
    imfp    = (math.sqrt(2)*math.pi*Constants.dH2**2*gas_n)  # here 1/lambda

    tau_s0 = 4./9. *math.sqrt(2/(Constants.meanw*Constants.mp*Constants.kB ) ) *math.pi *Constants.dH2**2

    itau_s  = sound / (rho_solid * asolid**2 *tau_s0 ) #TODO: check this formula! 

    itau_e  = gas_g * sound / (rho_solid * asolid ) 
    itau_stop = np.minimum(itau_e,itau_s) 

    n_stokes = O_gas / np.minimum(itau_e,itau_s)

 #   print("9/4 lambda_mfp=", 9./4.*mfp)
#
#    print("Stokes tau:", (1./itau_s)*O_gas)
#
#    print ("Eppstein tau:", (1./ itau_e)*O_gas)

    return itau_stop, v_gas, n_stokes, report_H
