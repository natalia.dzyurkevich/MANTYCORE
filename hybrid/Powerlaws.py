
import math
import numpy as np

def rand_powerlaw(slope, min_v, max_v):
    y = np.random.uniform()
    pow_max = pow(max_v, slope+1.)
    pow_min = pow(min_v, slope+1.)
    return pow((pow_max-pow_min)*y + pow_min, 1./(slope+1.))

def rand_uniform(minimum, maximum):  # TODO: get rid of this function
    return np.random.uniform(minimum, maximum)

def rand_rayleigh(sigma, size=None):
    return sigma*np.sqrt(-2*np.log(np.random.uniform(size=size)))

##def cor_bridges(r, v):
def eps_restitution(eps0): 
        eps = eps0    # 0.32 *pow(abs(v)*100.,-0.234)
        if eps>1.:
            eps=1.
        if eps<0.:
            eps=0.
        return eps

def cor_bridges(r, v):
        eps = 0.32 *pow(abs(v)*100.,-0.234)
        if eps>1.:
            eps=1.
        if eps<0.:
            eps=0.
        return eps
# usage:
#sim.coefficient_of_restitution = cor_bridges
