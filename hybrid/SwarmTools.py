
import math
import numpy as np

from hybrid import Constants
from hybrid.Utility import Args
from hybrid import Powerlaws as plaw


def generate_uniform_tracer(
        mTracer,
        aMin,
        aMax,
        dsize,
        eAvg,
        incAvg,
        size=None):
    """Generate uniformly distributed tracers.

    Parameters
    ----------
    :param mTracer:       mass of individual tracer (M⨀)  
    :param aMin:          minimum semimajor axis of swarms (AU)  
    :param aMax:          maximum semimajor axis of swarms (AU)  
    :param dsize:         radius of tracer body (AU)  
    :param eAvg:          average eccentricity  
    :param incAvg:        average inclination
    :param size:          shape of output array, optional
    """

    a     = np.random.uniform(aMin, aMax, size=size)
    e     = plaw.rand_rayleigh(eAvg, size=size)  # np.random.uniform(0., eMax, size=size)
    inc   = plaw.rand_rayleigh(incAvg, size=size)  # np.random.uniform(0., incMax, size=size)
    omega = np.random.uniform(0., 2.*math.pi, size=size)
    Omega = np.random.uniform(0., 2.*math.pi, size=size)
    f     = np.random.uniform(0., 2.*math.pi, size=size)
    if size is not None:
        return np.vectorize(lambda m,a,e,inc,Omega,omega,f: Args(m=mTracer, a=a, e=e, inc=inc, Omega=Omega, omega=omega, f=f))(mTracer, a, e, inc, Omega, omega, f)
    else:
        return Args(m=mTracer, a=a, e=e, inc=inc, Omega=Omega, omega=omega, f=f)

def generate_dizzy_swarm( # TODO: suggestions for better names are appreciated
        MSwarm,
        r,
        rad,
        dr,
        dv):
    """Generate uniformly distributed swarm.

    Parameters
    ----------
    :param Mswarm:        mass of individual swarm (Msun)  
    :param r:             radius of swarm/representative particle/planetesimal  
    :param rad:           average radial position of swarm ring (AU)  
    :param dr:            swarm dispersion radius /annulus (AU)  
    :param dv:            randomly oriented local velocity (AU/(years/2π))
    """

    rad = np.random.uniform(rad - dr/2, rad + dr/2)
    φ = np.random.uniform(0, 2*math.pi)
    vKt = math.sqrt(Constants.G*1/rad) # M=Msun (Msun)

    # cf. https://mathworld.wolfram.com/SpherePointPicking.html
    vRφ = np.random.uniform(0, 2*math.pi)
    vRz = np.random.uniform(-1, 1)
    vRp = math.sqrt(1 - vRz**2)
    zz  = np.random.uniform(-1,1)

    return Args(
         m=MSwarm,
         r=r,
         x=  rad* math.cos(φ),
         y=  rad* math.sin(φ),
         z=  zz * dv * 0.5*math.pi*rad/vKt, # random location, to be reached with dv in quarter orbital time
         vx=vKt*(-math.sin(φ))*(1  +  dv*vRp*math.cos(vRφ)),
         vy=vKt*( math.cos(φ))*(1  +  dv*vRp*math.sin(vRφ)),
         vz=vKt*(                     0.5*dv*vRz) )


