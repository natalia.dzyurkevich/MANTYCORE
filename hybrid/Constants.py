
import math

# Constants

year_s = 3.154e+7                 # s
T_s    = year_s / (2*math.pi)     # s
AU_cm  = 1.496e+13                # cm

Msun_g = 1.989e+33                # g

Mearth_g     = 5.9722e+27         # g
Mearth       = Mearth_g/Msun_g    # Msun
Rearth_cm    = 6.378e+8           # cm
Rearth_AU    = Rearth_cm/AU_cm
Rjupiter_cm  = 69.911e+8          # cm
Rjupiter_AU  = Rjupiter_cm/AU_cm
Mjupiter     = 1./1047.355        # in solar mass 
Rmoon_cm     = 1.737e+8           # cm
Mmoon_g      = 7.17e+25           # g
Rmoon_AU     = Rmoon_cm/AU_cm
Mmoon        = Mmoon_g/Msun_g


G_cgs  = 6.674e-8                # cm^3/(gs^2)
G      = 1.                      # AU^3 /Msun /year^2

#v_unit = T_s / AU_cm             # multiply by v_unit to transfer from cgs to AU/orbit velocity units
v_unit  =  1.0/math.sqrt(G_cgs * Msun_g / AU_cm)

dH2   = 2.76e-8                  # diameter of H2 molecule, in cm
mp    = 1.67e-24                 # protom mass
meanw = 2.33                     # mean molecular weight
kB    = 1.3807e-16               # Bolzmann's constant


