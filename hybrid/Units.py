
import math
from hybrid import Constants


class Quantity:

    def __init__(self,
            base_unit,
            name='',
            units={}):
        self.base_unit = base_unit
        self.name = name
        units[base_unit] = 1.
        self.units = units

    def to_unit(self, value, unit):
        return value/self.units[unit]

    def format_as_unit(self, value, unit, fmt='{}', sep=' '):
        return '{}{}{}'.format(fmt.format(value/self.units[unit]), sep, unit)

    def from_unit(self, value, unit):
        return value*self.units[unit]


cm = Quantity('cm', name='length',
    units={ 'nm': 1.e-7, 'µm': 1.e-4, 'mm': 1.e-1, 'm': 1.e+2, 'km': 1.e+5, 'AU': Constants.AU_cm })
g = Quantity('g', name='weight',
    units={ 'kg': 1.e+3, 'MSun': Constants.Msun_g, 'M⨀': Constants.Msun_g, 'MEarth': Constants.Mearth_g, 'M♁': Constants.Mearth_g, 'M⨁': Constants.Mearth_g })
s = Quantity('s', name='time',
    units={ 'years': Constants.year_s, 'yr': Constants.year_s, 'year': Constants.year_s, 'tK': Constants.T_s })
cm_s = Quantity('cm/s', name='velocity',
    units={ 'm/s': 100., 'km/s': 1.e+5 })
g_cm3 = Quantity('g/cm³', name='volume density')
g_cm2 = Quantity('g/cm²', name='surface density')
rad = Quantity('', name='angle',
    units={ 'deg': math.pi/180, '°': math.pi/180 })

AU = Quantity('AU', name='length',
    units={ 'nm': 1.e-7/Constants.AU_cm, 'µm': 1.e-4/Constants.AU_cm, 'mm': 1.e-1/Constants.AU_cm, 'cm': 1/Constants.AU_cm, 'm': 1.e+2/Constants.AU_cm, 'km': 1.e+5/Constants.AU_cm })
MSun = Quantity('M⨀', name='mass',
    units={ 'g': 1/Constants.Msun_g, 'kg': 1.e+3/Constants.Msun_g, 'MSun': 1., 'MEarth': Constants.Mearth_g/Constants.Msun_g, 'm♁': Constants.Mearth_g/Constants.Msun_g })
tK = Quantity('tK', name='time',
    units={ 's': 1/Constants.T_s, 'years': Constants.year_s/Constants.T_s, 'yr': Constants.year_s/Constants.T_s, 'year': Constants.year_s/Constants.T_s })
