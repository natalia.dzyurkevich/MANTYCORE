
#import math
import numpy as np
from hybrid import disk_parameters as diskparam
from hybrid import DiskTools
from hybrid import Constants

#### def my_dragforce(sim,dSwarm_cgs,MSwarm_g,dEmbryo_cgs,MEmbryo_g,NSwarm):
def my_dragforce(sim,dSwarm_cgs):    

    def f(lpSim):  # TODO: this argument is passed by REBOUND, but what is it? We only know it isn't identical to `sim`.
        ps = sim.particles
        NPlanet = diskparam.Nplanet
        #NEmbryo = sim.N - NPlanet - NSwarm
        for i in range(2,sim.N):
            #if i >= NEmbryo and i < (sim.N - NSwarm) :
            #   mass_obj   = MEmbryo_g
            #  asolid_obj = dEmbryo_cgs
            #if (i >= (sim.N - NSwarm) ):
            #   mass_obj   = MSwarm_g
            #  asolid_obj = dSwarm_cgs
            mass_obj = ps[i].m * Constants.Msun_g               # use cgs units here!
            asolid_obj = dSwarm_cgs # ps[i].r * Constants.AU_cm

            #print('loop over i=',i, ps[i].x, ps[i].y, ps[i].z)
            radSwarm = np.sqrt(ps[i].x**2+ps[i].y**2)
            zSwarm   = ps[i].z
    
            itau_stop, v_gas, nstokes, rep_H = DiskTools.calculate_stoppingtime(
               mass       = mass_obj,
               asolid     = asolid_obj,
               radSwarm   = radSwarm,
               zSwarm     = zSwarm
               ) #
            vkepl = np.sqrt(Constants.G*diskparam.Mcenter/radSwarm)
            #print ('vkepl vs vgas', vkepl, v_gas, (vkepl-v_gas))
            vgas_z = 0.0
            vgas_x = -v_gas / radSwarm * ps[i].y
            vgas_y =  v_gas / radSwarm * ps[i].x
            #print ('ps(i): vz     : ', i, ': ', vgas_z, ps[i].vz, itau_stop )
            #print ('ps(i): dvx,dvy: ', i, ': ', (vgas_x-ps[i].vx),  (vgas_y-ps[i].vy) )
            #print ('ps(i): ax     : ', i, ': ', ps[i].ax, ((ps[i].vx-vgas_x) * itau_stop*Constants.T_s) )
            #print ('ps(i): ay     : ', i, ': ', ps[i].ay, ((ps[i].vy-vgas_y) * itau_stop*Constants.T_s) )
            #print ('ps(i): az     : ', i, ': ', ps[i].az, ((ps[i].vz-vgas_z) * itau_stop*Constants.T_s) )

            # itau_stop = 0.0  # very hard, for testing only
            ps[i].ax -= (ps[i].vx-vgas_x) * itau_stop*Constants.T_s
            ps[i].ay -= (ps[i].vy-vgas_y) * itau_stop*Constants.T_s
            ps[i].az -= (ps[i].vz-vgas_z) * itau_stop*Constants.T_s

    return f
