
import mc_coag

import hybrid.Constants as Constants


def orbit_family_simulator():
    """Create a RPMC orbit family simulator.

    Parameters
    ----------
    """

    return mc_coag.disk2.OrbitFamilySimulator()


def orbit_surface_family_simulator():
    """Create a RPMC orbit surface family simulator.

    Parameters
    ----------
    """

    return mc_coag.diskS.OrbitFamilySSimulator()


def grid_coagulation_simulator(rMin, dr, Ncells):
    """Create a RPMC radial ensemble grid simulator.

    Parameters
    ----------
    :param rMin:    minimum cell radius (AU)  
    :param dr:      radial cell size (AU)  
    :param Ncells:  how many cells to use
    """

    collisionRate = mc_coag.disk.ComputeCollisionRate_Mechanical(
         M=Constants.Msun_g)
    collide = mc_coag.Coagulate()
    collisionModel = mc_coag.disk.CollisionModel(collisionRate, collide)

    cellSimulator = mc_coag.disk.EnsembleSimulator(
        collisionModel=collisionModel)

    return mc_coag.disk.RadialEnsembleGridSimulator(
        cellSimulator=cellSimulator,
        rMin=rMin * Constants.AU_cm,
        dr=dr * Constants.AU_cm,
        numCells=Ncells)
