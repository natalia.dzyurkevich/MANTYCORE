
import math

# those parameters are needed to define  a static gas profile in the disk IRS 48 

# Star
Mcenter = 2.5         # mass of central star, in Solar masses
Lcenter = 42          # its luminosity
Rcenter = 12          # stellar radius

Rad0         = 66             # AU  radius of the dust ring
dR_ring      = 6              # AU  thickness of the dust ring

# Planet(s)
Nplanet = 1
Mplanet = 1                   # in Jupiter mass
ajupiter_AU  = 42.56          # AU  locate the planet
put_gap      = 0.0            # planet-carved gap requires put_gap=1. 
gapfloor     = 0.4            # Pinilla 2012, alpha = 0.01  
amoon_AU     = 66             # AU   locate the embryo

# gas temperature profile

rref  = 60.0          # AU

Temperature0 = 116.   # K
q_factor = 0. # 0.5        # radial slope

# gas profile

Sigma0 = 500.           # in g/cm**2
p_factor = 1.5  # 1.         # radial slope


# assign fixed timestep, so that drag force is resolved (typically 1/1000 inner orbit, see Rein 2012, no reason given)

timestep_fix = 0.0001

