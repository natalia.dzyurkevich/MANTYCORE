
import math
import numpy as np

from hybrid import Constants
from hybrid.Utility import Args
from hybrid import Powerlaws as plaw


def generate_uniform_embryo(
        MEmbryo,
        rEMin,
        rEMax,
        #orbitalRange,
        rE, 
        eEmbryo,
        incEmbryo):
    """Generate uniformly distributed swarm.

    Parameters
    ----------
    :param Mswarm:        mass of individual swarm (Msun)  
    :param rMin:          minimum swarm radius (AU)  
    :param rMax:          maximum swarm radius (AU)  
    :param orbitalRange:  TODO what exactly is this?  
    :param r:             radius of each planetesimal        
    :param eMax:          maximum eccentricity  
    :param incMax:        maximum inclination
    """

    Orbital = np.random.uniform(rEMin, rEMax)
    M     = np.random.uniform(0., 2.*math.pi) 
    omega = np.random.uniform(0., 0.2*math.pi) 
    Omega = np.random.uniform(0., 0.2*math.pi) 
    theta = np.random.uniform(0., 0.2*math.pi)
    #orbital = math.sqrt(Constants.G/r**3) * (1. + np.random.uniform(-orbitalRange, orbitalRange)) # TODO: ???
    
    e     = plaw.rand_rayleigh(eEmbryo)    ## np.random.uniform(0., eMax)
    inc   = plaw.rand_rayleigh(incEmbryo)   ## np.random.uniform(0., incMax)
    return Args(m=MEmbryo, a=Orbital,  e=e, inc=inc, r=rE, Omega=Omega, omega=omega, theta=theta  )

def generate_dizzy_embryo( # TODO: suggestions for better names are appreciated
        MEmbryo,
        rE,
        radE,
        drEmbryo,
        dvEmbryo):
    """Generate uniformly distributed swarm.

    Parameters
    ----------
    :param Mswarm:        mass of individual swarm (Msun)  
    :param r:             radius of swarm/representative particle/planetesimal
    :param rad:           average radial position of swarm ring (AU)  
    :param dr:            swarm dispersion radius /annulus (AU)  
    :param dv:            randomly oriented local velocity (AU/(years/2π))
    """

    rad = np.random.uniform(radE - drEmbryo/2, radE + drEmbryo/2)
    φ = np.random.uniform(0, 2*math.pi)
    vKt = math.sqrt(Constants.G*1/radE) # M=Msun (Msun)

    # cf. https://mathworld.wolfram.com/SpherePointPicking.html
    vRφ = np.random.uniform(0, 2*math.pi)
    vRz = np.random.uniform(-1, 1)
    vRp = math.sqrt(1 - vRz**2)
    zz  = np.random.uniform(-1,1)

    return Args(
         m=MEmbryo,
         r=rE,
         x=  radE* math.cos(φ),
         y=  radE* math.sin(φ),
         z=  zz * dv * 0.5*math.pi*rad/vKt, # random location, to be reached with dv in quarter orbital time
         vx=vKt*(-math.sin(φ))*(1  +  dvEmbryo*vRp*math.cos(vRφ)),
         vy=vKt*( math.cos(φ))*(1  +  dvEmbryo*vRp*math.sin(vRφ)),
         vz=vKt*(                     0.5*dvEmbryo*vRz) )


