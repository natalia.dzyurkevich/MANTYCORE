
import math
from hybrid import disk_parameters as diskparam
from hybrid import Constants
from hybrid import DiskTools 

def timescales(dv0,Ntracer,Mtracer_g, dtracer_cgs ):

   # orbital time, typically of largest object in disk, aka planet
   a_choice   =  diskparam.Rad0         # ring location 
   da         =  diskparam.dR_ring      # half-thickness of the ring
   #a_choice  =  diskparam.ajupiter_AU  # planet
   omega      = math.sqrt(diskparam.Mcenter*Constants.G/a_choice**3)
   omega_cgs  = omega * math.sqrt(Constants.Msun_g*Constants.G_cgs/Constants.AU_cm**3)

   tau_orb    = 2.*math.pi / omega_cgs

   # gravitational scattering (_Gs)
   vkepl_cgs     =  a_choice*Constants.AU_cm * omega_cgs           # mean velocity
   vturb         =  dv0 * vkepl_cgs                                # turbulent velocity; dv0 is turb velocity relative to mean
   volume_ring   =  a_choice*dv0/2. * math.pi*((a_choice+da)**2 -(a_choice-da)**2)*(Constants.AU_cm)**3
   density_cm3   =  Ntracer / volume_ring                          # mean number density of planetesimals in 1/cm^3 across the ring volume
   sigmagrav     =  (Constants.G_cgs * Mtracer_g / (vturb)**2  )**2 

   tau_Gs        = 1./(dv0*vkepl_cgs * density_cm3 * sigmagrav)

   # timescale of gravitational instability for ring of planetesimals

   tau_GI        = 1./math.sqrt( Constants.G_cgs * density_cm3 * Mtracer_g )

   tau_coll      = 1./ (2.*math.pi * dtracer_cgs**2 * vturb * density_cm3 )


   # timescale for gas drag force
   itau_stop, v_gas, nstokes, rep_H = DiskTools.calculate_stoppingtime(
       mass       = Mtracer_g,     #/Constants.Msun_g,
       asolid     = dtracer_cgs,
       radSwarm   = a_choice,
       zSwarm     = 0 )
   
   tau_drag = 1./ itau_stop

   StokesN  = tau_drag * omega_cgs

   print ('My numbers:for tracer parameters in cgs: mass, d, number:', Mtracer_g, dtracer_cgs, Ntracer)
   print ('Stokes number: ', nstokes)
   print ('Gravitational stirring :', tau_Gs*omega_cgs)
   print ('Gravitational Instability:', tau_GI*omega_cgs)
   print ('Collisional timescale:', tau_coll*omega_cgs)

   # useful scales, follow Ormel & Dullemond (2010)
   rHill      = a_choice * (Mtracer_g / (3*Constants.Msun_g * diskparam.Mcenter))**(1./3.)
   rHill_cgs  = rHill * Constants.AU_cm
   vHill      = rHill * omega * a_choice
   vHill_cgs  = rHill_cgs * vkepl_cgs
   vesc_cgs   = math.sqrt( 2. * Mtracer_g / dtracer_cgs)                 # TODO: or use 2.*dswarm?
   print ('Hill radius, cgs:', rHill_cgs)
   print ('Hill velocity, cgs:', vHill_cgs)
   print ('esc velocity, cgs:', vesc_cgs)


   return tau_orb, tau_Gs, tau_GI, tau_coll, StokesN

