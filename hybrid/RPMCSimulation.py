
import math
import numpy as np
import pandas as pd

import mc_coag

import hybrid.Constants as Constants
from hybrid.Utility import Args


# particle types recognized by mc-coag
ptPassive = 0      # particle neither accretes masses nor represents swarm (e.g. stars, massive planets)
ptParticle = 1     # accretes masses from swarms but does not represent swarm (e.g. embryo)
ptSwarmTracer = 2  # accretes masses, represents swarm of equivalent particles


class RPMCSimulation:
    """Representative Particle Monte Carlo simulation"""

    def __init__(self,
            simulator,
            ids,
            types,
            masses,
            representedMasses,
            dsizes,
            randSeed,
            suppress = False):
        """Sets up a Representative Particle Monte Carlo simulation with the given parameters

        Parameters
        ----------
        :param simulator:          simulator class  
        :param ids:                range of particle ids  
        :param types:              range of particle types  
        :param masses:             individual particle masses (scalar or array)  
        :param representedMasses:  masses represented by individual particle (scalar or array)  
        :param dsizes:             individual particle radii (scalar or array)  
        :param randSeed:           random number seed  
        :param suppress:           make RPMC simulation run as no-op
        """

        self._suppress = suppress
        
        # allocate simulation data
        _ids = np.array(ids, dtype=np.int32)
        ones = np.ones([len(_ids)], dtype=np.float)
        ones_i8 = np.ones([len(_ids)], dtype=np.int8)
        self._state = pd.DataFrame.from_dict({
            'id':              pd.Series(_ids, dtype=np.int32),
            'type':            pd.Series(ones_i8*types, dtype=np.int8),
            'mass':            pd.Series(ones*masses, dtype=np.float),
            'representedMass': pd.Series(ones*representedMasses, dtype=np.float),
            'dsize':           pd.Series(ones*dsizes, dtype=np.float),
            'x':               pd.Series(0, dtype=np.float),
            'y':               pd.Series(0, dtype=np.float),
            'z':               pd.Series(0, dtype=np.float),
            'vx':              pd.Series(0, dtype=np.float),
            'vy':              pd.Series(0, dtype=np.float),
            'vz':              pd.Series(0, dtype=np.float)
        })
        #print("(init) column addresses: &m={:02x} &x={:02x}\n".format(self._state.mass.to_numpy().__array_interface__['data'][0], self._state.x.to_numpy().__array_interface__['data'][0]))
        _ids = None

        self._sim = simulator.allocateSimulation(
            randSeed=randSeed,
            data=self._state)


    def integrate_to(self, t):
        """Integrates the simulation until time `t`.

        Parameters
        ----------
        :param t:  time (years)
        """

        if not self._suppress:
            return self._sim.advanceTo(t)
        else:
            return t,0


    def set_particle_phasespace_state(self, state):
        """Updates the particle phase-space coordinates.

        Parameters
        ----------
        :param state:  particle state
        """

        # gather coordinates and velocities
        #print("(pre-update) column addresses: &m={:02x} &x={:02x}\n".format(self._state.mass.to_numpy().__array_interface__['data'][0], self._state.x.to_numpy().__array_interface__['data'][0]))
        ids = self._state.id.array
        self._state.x.to_numpy()[:] = state.x.array[ids]
        self._state.y.to_numpy()[:] = state.y.array[ids]
        self._state.z.to_numpy()[:] = state.z.array[ids]
        self._state.vx.to_numpy()[:] = state.vx.array[ids]
        self._state.vy.to_numpy()[:] = state.vy.array[ids]
        self._state.vz.to_numpy()[:] = state.vz.array[ids]
        #print("set RPMC state:\n", state, "\n", self._state, "\n")
        #print("(post-update) column addresses: &m={:02x} &x={:02x}\n".format(self._state.mass.to_numpy().__array_interface__['data'][0], self._state.x.to_numpy().__array_interface__['data'][0]))

        if not self._suppress:
            return self._sim.initialize()
        else:
            return 0.

    def get_particle_phasespace_state(self, state):
        """Retrieves the particle phase-space coordinates.

        Parameters
        ----------
        :param state:  particle state
        """

        if not self._suppress:
            self._sim.finalize()

        # scatter coordinates and velocities
        ids = self._state.id.array
        state.x.to_numpy()[ids] = self._state.x.array
        state.y.to_numpy()[ids] = self._state.y.array
        state.z.to_numpy()[ids] = self._state.z.array
        state.vx.to_numpy()[ids] = self._state.vx.array
        state.vy.to_numpy()[ids] = self._state.vy.array
        state.vz.to_numpy()[ids] = self._state.vz.array
        #print("get RPMC state:\n", state, "\n", self._state, "\n")


    @property
    def state(self):
        """Returns the current simulation state.

        The state is a dataframe with the following columns:  
        `id`:               particle index  
        `type`:             particle type  
        `mass`:             particle mass (Msun)  
        `representedMass`:  mass represented by particle (Msun)  
        `dsize`:            particle radius (AU)  
        `x`:                x particle position (AU)  
        `y`:                y particle position (AU)  
        `z`:                z particle position (AU)  
        `vx`:               x particle velocity (AU/T)  
        `vy`:               y particle velocity (AU/T)  
        `vz`:               z particle velocity (AU/T)
        """
        return self._state
