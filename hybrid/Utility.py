
import re
import sys
import types
import argparse
import functools


class Args:
    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
        self.__dict__.update(kwargs)


class Param:

    @staticmethod
    def _dtype_fmt(dtype):
        if dtype is float:
            return '{:g}'
        else:
            return '{}'

    @staticmethod
    def _dtype_parse_re(dtype):
            return '.*?'  # non-greedy

    @staticmethod
    def _typeof(x):
        return type(x)

    def __init__(self,
            name=None,  # : str? | [str]?
            short=None,  # : str?
            default=None,  # : str?
            unit=None,  # : str?
            quantity=None,  # : Units.Quantity?
            type=None,  # : type?
            parse_re=None,  # : str?
            flags=None,  # : []?
            help=''):  # : str
        if flags is None:
            flags = []
        assert name is not None or short is not None
        if short is not None:
            if Param._typeof(short) is not str:
                # assume list of names
                flags += ['-' + s for s in short]
                short = short[0]
            else:
                flags.append('-' + short)
        if name is not None:
            if Param._typeof(name) is not str:
                # assume list of names
                flags += ['--' + n for n in name]
                name = name[0]
            else:
                flags.append('--' + name)
        else:
            name = short
        self.name = name
        self.short = short
        argtype = type
        if quantity is not None:
            assert unit is None or unit in quantity.units
            assert type is None
            assert parse_re is None
            type = float
            argtype = str
            if unit is None:
                unit = quantity.base_unit
            unitPattern = '|'.join([re.escape(u) for u in quantity.units.keys()])
        else:
            if unit is None:
                unit = ''
            unitPattern = re.escape(unit)
        self.unit = unit
        if type is None and default is not None:
            type = Param._typeof(default)
        assert type is not None
        if argtype is None:
            argtype = type
        self.type = type
        self.argtype = argtype
        self.quantity = quantity
        if parse_re is None:
            parse_re = Param._dtype_parse_re(type)
        self.value_str_parser = re.compile('^\\s*{}\\s*[:=]\\s*(.*)$'.format(name))
        #self.parser = re.compile('\\s*{}\\s*[:=]\\s*({})\\s*({})'.format(name, parse_re, unitPattern))
        self.value_parser = re.compile('^({})\\s*({})$'.format(parse_re, unitPattern))
        if default is not None and self.quantity is not None:
            default_str = default
            default = self.deserialize_value(default_str)
        else:
            default_str = str(default) if default is not None else None
        self.default_str = default_str
        self.default = default
        self.flags = flags
        self.help = help

    def is_default(self, value):
        #return self.default is not None and value == self.default
        return value == self.default

    def serialize_value(self, value, unit_sep='', fmt=None):
        if value is None:
            value = self.default
        if value is None:
            return 'None'
        else:
            if fmt is not None and Param._typeof(value) in fmt:
                fmt = fmt[Param._typeof(value)]
            else:
                fmt = Param._dtype_fmt(Param._typeof(value))
            if self.quantity is not None:
                value = self.quantity.to_unit(value=value, unit=self.unit)
            valueStr = fmt.format(value)
            return '{}{}{}'.format(valueStr, unit_sep, self.unit)

    def serialize(self, value, sep='=', short=False, unit_sep='', fmt=None):
        if short and self.short is not None:
            paramName = self.short
        else:
            paramName = self.name
        valueStr = self.serialize_value(value, unit_sep=unit_sep, fmt=fmt)
        return '{}{}{}'.format(paramName, sep, valueStr)

    def deserialize_value(self, valueStr):
        match = self.value_parser.match(valueStr)
        if match is None:
            raise Exception('Parameter \'{}\': cannot parse argument \'{}\': unrecognized syntax or unknown unit'.format(self.name, valueStr))
        groups = match.groups()
        unit = groups[1]
        value = self.type(groups[0])
        if self.quantity is not None:
            value = float(value)
            assert unit in self.quantity.units  # otherwise the regex shouldn't have matched
            value = self.quantity.from_unit(value=value, unit=unit)
        else:
            if unit != self.unit:
                raise Exception('Parameter \'{}\': unit mismatch: got \'{}\' but expected \'{}\''.format(self.name, unit, self.unit))
        return value

    def deserialize(self, nameValueStr):
        match = self.value_str_parser.match(nameValueStr)
        if match is None:
            raise Exception('Parameter \'{}\': cannot parse argument \'{}\': unrecognized syntax or wrong parameter'.format(self.name, nameValueStr))
        valueStr = match.groups()[0]
        return self.deserialize_value(valueStr)


# taken from https://stackoverflow.com/a/31174427 and extended
def rhasattr(obj, attr):
    for subattr in attr.split('.'):
        if not hasattr(obj, subattr):
            return False
        obj = getattr(obj, subattr)
    return True
def rgetattr(obj, attr, *args):
    def _getattr(obj, attr):
        return getattr(obj, attr, *args)
    return functools.reduce(_getattr, [obj] + attr.split('.'))
def rsetattr(obj, attr, val):
    pre, _, post = attr.rpartition('.')
    return setattr(rgetattr(obj, pre) if pre else obj, post, val)
def rgetattr_force(obj, attr, attr_factory=lambda _obj,_attr: types.SimpleNamespace()):
    def _getattr_force(obj, attr):
        if hasattr(obj, attr):
            return getattr(obj, attr)
        else:
            newattr = attr_factory(obj, attr)
            setattr(obj, attr, newattr)
            return newattr
    return functools.reduce(_getattr_force, [obj] + attr.split('.'))
def rsetattr_force(obj, attr, val):
    pre, _, post = attr.rpartition('.')
    return setattr(rgetattr_force(obj, pre) if pre else obj, post, val)


class Params:

    _nameParser = re.compile('\\s*([A-Za-z_][A-Za-z0-9_-]*)\\s*[:=]')

    def __init__(self, params):
        self._params = list(params)
        self._param_dict = dict((param.name, param) for param in self._params)
        for param in self._params:
            if param.short is not None:
                memberName = param.short.replace('-', '_')
                self._param_dict[memberName] = param

    def _enumerate(self, args, omit_defaults, subset=None):
        for param in self._params:
            if subset is None or param.name in subset:
                paramName = param.name
                memberName = paramName.replace('-', '_')
                if hasattr(args, memberName):
                    value = getattr(args, memberName)
                else:
                    value = param.default
                #if value is not None and (value != param.default or not omit_defaults):
                if value != param.default or not omit_defaults:
                    yield param, value

    def _serialize(self, args, prefix, kv_sep, param_sep, unit_sep, omit_defaults, subset, short,):
        result = ''
        for param, value in self._enumerate(args=args, omit_defaults=omit_defaults, subset=subset):
            result += prefix + param.serialize(value, kv_sep, short=short, unit_sep=unit_sep) + param_sep
        return result

    def _deserialize(self, argsString, prefix, param_sep):
        result = Args()
        argStrings = argsString.split(param_sep)
        for argString in argStrings:
            if argString.startswith(prefix):
                argString = argString[len(prefix):]
                match = Params._nameParser.match(argString)
                if match is None:
                    break # skip over illegible lines (e.g. comment lines)
                    #raise Exception('Cannot parse argument \'{}\': unrecognized syntax'.format(argString))
                paramOrShortName = match.groups()[0]
                if paramOrShortName in self._param_dict: # ignore parameter if unknown
                    param = self._param_dict[paramOrShortName]
                    memberName = param.name.replace('-', '_')
                    value = param.deserialize(argString)
                    setattr(result, memberName, value)
        for param in self._params:
            memberName = param.name.replace('-', '_')
            if not hasattr(result, memberName) and param.default is not None:
                setattr(result, memberName, param.default)
        return result

    def serialize_string(self, args, omit_defaults=False, subset=None, short=True):
        return self._serialize(args, prefix='', kv_sep='=', param_sep=' ', unit_sep='', omit_defaults=omit_defaults, subset=subset, short=short).rstrip()

    def deserialize_string(self, argsString):
        return self._deserialize(argsString, prefix='', param_sep=' ')

    def serialize_lines(self, args, prefix='# ', omit_defaults=False, subset=None):
        return self._serialize(args, prefix=prefix, kv_sep=': ', param_sep='\n', unit_sep=' ', omit_defaults=omit_defaults, subset=subset, short=False)

    def deserialize_lines(self, argsString, prefix='#'):
        return self._deserialize(argsString, prefix=prefix, param_sep='\n')

    def to_file(self, args, file, mode='wb', prefix='#', omit_defaults=False, subset=None):
        with open(file, mode=mode) as textFile:
            textFile.write(self.serialize_lines(args, prefix=prefix, omit_defaults=omit_defaults, subset=subset).encode('utf8'))

    def from_file(self, file, prefix='#', omit_defaults=False, subset=None):
        lines = b''
        with open(file, mode='rb') as textFile:
            for line in textFile:
                #print (prefix, format(ord(prefix), 'b'))
                print (line)
                #if not line.startswith(format(ord(prefix),'b')):
                if not line.startswith(b'#'):
                    break
                lines += line + b'\n'
        print ('lines=',lines.decode('utf8'))       
        return self.deserialize_lines(lines.decode('utf8'), prefix=prefix)

    class _StoreAction(argparse.Action):
        def __init__(self, *args, dest, **kwargs):
            super(Params._StoreAction, self).__init__(*args, dest=argparse.SUPPRESS, **kwargs)
            self.rdest = dest
        def __call__(self, parser, namespace, values, option_string=None):
            rsetattr_force(namespace, self.rdest, values)

    class _StoreQuantityAction(argparse.Action):
        def __init__(self, *args, dest, quantity_param, **kwargs):
            super(Params._StoreQuantityAction, self).__init__(*args, dest=argparse.SUPPRESS, **kwargs)
            self.rdest = dest
            self.quantity_param = quantity_param
        def __call__(self, parser, namespace, values, option_string=None):
            numericValue = self.quantity_param.deserialize_value(values)
            rsetattr_force(namespace, self.rdest, numericValue)

    def init_default_args(self, namespace):
        for param in self._params:
            memberName = param.name.replace('-', '_')
            if not rhasattr(namespace, memberName):
                rsetattr_force(namespace, memberName, param.default)
        return namespace

    def add_to_argparser(self, argparser):

        for param in self._params:
            if param.quantity is not None:
                action = Params._StoreQuantityAction
                actionArgs = { 'quantity_param': param }
            else:
                action = Params._StoreAction
                actionArgs = {}
            memberName = param.name.replace('-', '_')
            help = param.help
            if param.quantity is None and param.unit != '':
                help += ' ({}'.format(param.unit)
                if param.default is not None:
                    help += '; default: {} {}'.format(param.default, param.unit)
                help += ')'
            elif param.default is not None and param.default_str != '':
                help += ' (default: {})'.format(param.default_str)  # we could report the default value in the given unit for quantities, but let's not bother
            argparser.add_argument(
                *param.flags, default=param.default, type=param.argtype, help=help, dest=memberName,
                action=action, **actionArgs)

    def report(self, args, prefix='- ', help_sep=':  ', kv_sep=' = ', unit_sep=' ', fmt=None, file=sys.stdout, omit_defaults=False):
        for param, value in self._enumerate(args=args, omit_defaults=omit_defaults):
            print('{}{}{}{}'.format(prefix, param.help, help_sep, param.serialize(value, kv_sep, short=False, unit_sep=unit_sep, fmt=fmt)), file=file)
