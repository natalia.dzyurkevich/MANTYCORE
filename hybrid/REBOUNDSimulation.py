
import math
import numpy as np
import pandas as pd

# REBOUND doesn't compile on Windows, so we'll fake it for the purpose of being able to debug RPMC on Windows
import platform
haveREBOUND = platform.system() != 'Windows'
if haveREBOUND:
    import rebound

from hybrid import Constants
from hybrid import Powerlaws as plaw


class REBOUNDSimulation:
    """N-body simulation implemented with REBOUND"""

    def __init__(self,
            sun,
            planets = [],
            tracers = [],
            suppress = False):
        """Sets up an N-body simulation with the given properties

        Parameters
        ----------
        :param sun:       central object  
        :param planets:   planets  
        :param tracers:   tracers  
        :param suppress:  make N-body simulation run as no-op
        """

        self._sim_t = 0.0
        self._suppress = suppress
        if not suppress and not haveREBOUND:
            raise Exception('cannot run N-body simulation if REBOUND is not available')

        if not suppress:
            self._sim = rebound.Simulation()

            # set parameters (defaults are G=1, t=0, dt=0.01)
            self._sim.G = Constants.G

            # add objects
            self._sim.add(**sun.kwargs)
            for planet in planets:
                self._sim.add(**planet.kwargs)
            for tracer in tracers:
                self._sim.add(**tracer.kwargs)

            #self._sim.ri_ias15.min_dt =  1.e-3
            #self._sim.integrator = "whfast"
            # set the center of momentum to be at the origin
            self._sim.move_to_com()
            #self._sim.move_to_hel()

        self.N = 1 + len(planets) + len(tracers)

        # allocate state dataframe
        self._state_t = None
        columns = ['m', 'x', 'y', 'z', 'r', 'vx', 'vy', 'vz', 'vr', 'a', 'e', 'inc']
        stateData = np.zeros(shape=[self.N, len(columns)], dtype=np.float, order='F')
        self._state = pd.DataFrame(stateData, columns=columns)

        if suppress:
            import mc_coag
            mc_coag.diskS.convertFromOrbitalParams(self._state, [sun] + planets + tracers)
            #print("state(init):\n", self._state)

    @property
    def sim(self):
        """Returns the `rebound.Simulation` object."""
        return self._sim if not self._suppress else None

    def _capture_state(self):
        if not self._suppress:
            m = self._state.m.array
            x = self._state.x.array
            y = self._state.y.array
            z = self._state.z.array
            r = self._state.r.array
            vx = self._state.vx.array
            vy = self._state.vy.array
            vz = self._state.vz.array
            vr = self._state.vr.array
            a = self._state.a.array
            e = self._state.e.array
            inc = self._state.inc.array

            m[0] = self._sim.particles[0].m  # (Msun)
            x[0] = self._sim.particles[0].x  # (AU)
            y[0] = self._sim.particles[0].y  # (AU)
            z[0] = self._sim.particles[0].z  # (AU)
            vx[0] = self._sim.particles[0].vx  # (AU/T)
            vy[0] = self._sim.particles[0].vy  # (AU/T)
            vz[0] = self._sim.particles[0].vz  # (AU/T)
            for i, p in enumerate(self._sim.particles[1:]):
                j = 1 + i
                m[j] = p.m  # (Msun)
                x[j] = p.x  # (AU)
                y[j] = p.y  # (AU)
                z[j] = p.z  # (AU)
                xr = p.x - x[0]
                yr = p.y - y[0]
                zr = p.z - z[0]
                r[j] = math.sqrt(xr**2 + yr**2 + zr**2)  # (AU)
                vx[j] = p.vx  # (AU/T)
                vy[j] = p.vy  # (AU/T)
                vz[j] = p.vz  # (AU/T)
                vxr = p.vx - vx[0]
                vyr = p.vy - vy[0]
                vzr = p.vz - vz[0]
                #vr[i] = (p.x * p.vx + p.y * p.vy) / r[i]  # (AU/T)
                vr[j] = math.sqrt(vxr**2 + vyr**2 + vzr**2)
                a[j] = p.a
                e[j] = p.e
                inc[j] = p.inc
        else:
            import mc_coag
            mc_coag.diskS.updateOrbitalParams(self._state)
        #print("state(capture):\n", self._state)

    def _update_state(self):
        if not self._suppress:
            m = self._state.m.array
            x = self._state.x.array
            y = self._state.y.array
            z = self._state.z.array
            vx = self._state.vx.array
            vy = self._state.vy.array
            vz = self._state.vz.array

            for i in range(1, self.N):
                self._sim.particles[i] = rebound.Particle(m=m[i], x=x[i], y=y[i], z=z[i], vx=vx[i], vy=vy[i], vz=vz[i])
            #print("state(update):\n", self._state)


    def integrate_to(self, t):
        """Integrates the simulation until time `t`.

        Parameters
        ----------
        :param t:  time (years)
        """

        if self._state_t is not None and self._state_t == self._sim_t:
            # state has been captured and potentially modified; update N-body state
            self._update_state()
            if not self._suppress:
                self._sim.integrate(t)
                #self._sim.move_to_com()
        self._sim_t = t


    @property
    def state(self):
        """Returns the current simulation state.
        
        The state is a dataframe with the following columns:  
        `m`:      particle mass (Msun)  
        `x`:      particle x position (AU)  
        `y`:      particle y position (AU)  
        `r`:      radial particle position (AU)  
        `z`:      vertical particle position (AU)  
        `vx`:     particle x velocity (AU/T)  
        `vy`:     particle y velocity (AU/T)  
        `vr`:     radial particle velocity (AU/T)  
        `vz`:     vertical particle velocity (AU/T)
        `a`:      semimajor axis
        `e`:      eccentricity
        `inc`:    inclination
        """

        # refresh the state lazily
        if self._state_t is None or self._state_t < self._sim_t:
            #print('capturing state (state_t: {}, sim_t: {})'.format(self._state_t, self._sim_t))
            #print('  before: a: {}, e: {}'.format(self._state.a.array, self._state.e.array))
            self._capture_state()
            #print('  after: a: {}, e: {}'.format(self._state.a.array, self._state.e.array))
            self._state_t = self._sim_t

        return self._state
