
import math


def run_nb_simulation(
        nbSim,
        times,
        captureNBState):
    """Runs a N-body simulation.

    Parameters
    ----------
    :param nbSim:            object representing the N-body simulation  
    :param times:            list of times at which to capture the simulation state (years)  
    :param captureNBState:   callback `captureNBState(i, t, nbState)` captures the state of the N-body simulation at timestep `i` and time `t`
    """
    
    for i, time in enumerate(times):
        nbSim.integrate_to(time)
        captureNBState(i, time, nbSim.state)


def run_hybrid_simulation(
        nbSim,
        mcSim,
        computeTimestep,
        times = None,
        timesNB = None,
        timesMC = None,
        captureNBState = None,
        captureMCState = None):
    """Runs a hybrid N-body/Monte Carlo simulation.

    Parameters
    ----------
    :param nbSim:            object representing the N-body simulation  
    :param mcSim:            object representing the Monte Carlo simulation  
    :param computeTimestep:  callback `computeTimestep(t, nbState, mcState)` computes the timestep  
    :param times:            list of times at which to capture the simulation state (years)  
    :param timesNB:          list of times at which to capture the N-body simulation state (years)  
    :param timesMC:          list of times at which to capture the Monte Carlo simulation state (years)  
    :param captureNBState:   callback `captureNBState(i, t, nbState)` captures the state of the N-body simulation at timestep `i` and time `t`  
    :param captureMCState:   callback `captureMCState(i, t, mcState)` captures the state of the Monte Carlo simulation at timestep `i` and time `t`  
    """
    
    if timesNB is None:
        timesNB = times
        if timesNB is None:
            timesNB = []
    if timesMC is None:
        timesMC = times
        if timesMC is None:
            timesMC = []

    itNB = iter(timesNB)
    itMC = iter(timesMC)

    t = 0.0
    iNB = 0
    iMC = 0
    tCaptureNB = next(itNB, None)
    tCaptureMC = next(itMC, None)
    while tCaptureNB is not None or tCaptureMC is not None:

        # compute new timestep
        dt = computeTimestep(t, nbSim.state, mcSim.state)
        tNext = t + dt

        # tick: advance MC simulation while particles are "frozen" into position
        totalRate = mcSim.set_particle_phasespace_state(nbSim.state)
        numCollisions = 0
        while tCaptureMC is not None and tCaptureMC <= tNext:
            _,nc = mcSim.integrate_to(tCaptureMC)
            numCollisions += nc
            if captureMCState is not None:
                captureMCState(iMC, tCaptureMC, mcSim.state)
            iMC += 1

            tCaptureMCPrev = tCaptureMC
            tCaptureMC = next(itMC, None)
            if tCaptureMC is None:
                break
            assert tCaptureMC >= tCaptureMCPrev, 'timesteps must be monotonic'
        _,nc = mcSim.integrate_to(tNext)
        numCollisions += nc
        print('[mc_coag] integrating to t={:.2f} yr: {} collisions, total collision rate was {} #/yr'.format(tNext / (2*math.pi), numCollisions, totalRate*(2*math.pi)))
        mcSim.get_particle_phasespace_state(nbSim.state)

        # tock: advance n-body simulation
        while tCaptureNB is not None and tCaptureNB <= tNext:
            nbSim.integrate_to(tCaptureNB)
            if captureNBState is not None:
                captureNBState(iNB, tCaptureNB, nbSim.state)
            iNB += 1

            tCaptureNBPrev = tCaptureNB
            tCaptureNB = next(itNB, None)
            if tCaptureNB is None:
                break
            assert tCaptureNB >= tCaptureNBPrev, 'timesteps must be monotonic'
        nbSim.integrate_to(tNext)

        t = tNext
