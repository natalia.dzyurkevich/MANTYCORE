# MANTYCORE

Merging Monte-cArlo and Nbody To hYbrid: planetary CORE formation project:  "MANTYCORE". 
Python-only version of hybridization.  

Developers: Natalia Dzyurkevich, Moritz Beutel. 

# Introduction 

**Astrophysical angle**: N body method  alone is treating gravitational interactions of up to several thousand massive bodies: star(s), planet(s), moon(s), rocks.  Creating a hybrid of N body with Monte Carlo coagulation code extends the limit on the number of solid bodies in the system to astronomical numbers (easily over 1.e+12), as the rocks are treated _statistically_. 

**Technical aspects**: We want to leave a free choice which package to use for N body, or for Monte Carlo coagulation, and each package should be available for use on its own using the same interface.

# Construction 

- run-NB-drag.py is a preliminary version of interface, in progress;
- hybrid/ contains the collection of tools: add gaseous disk with DistTools.py, gas drag force with dragforce.py, generate the swarm of rocks with SwarmTools.py, define what to simulate with Simulation.py;
- hybrid/Simulation.py lists the possibilities: run as N-body only, or as a hybrid;
- REBOUNDSimulation.py: N-body of choice is REBOUND https://rebound.readthedocs.io/en/latest/index.html ;
  Please install it first as 
          pip install rebound
- RPMCSimulation.py: Monte-Carlo package is "mc_coag" (not public);

# Examples of use

Properties os star and massive planets are added as a list of classes within run-NB-drag.py. The current version allows to pass quite a few arguments, for example duration (in orbital times) and number of rocks, in the command line:
   
./run-NB-drag.py --run --log --outdir 'your_exosystem'  --duration 'your_time' -N 'rocks_number' --write --plot

 It will store the data in *tsv file in "your_exosystem" directory, and plot eccentricities, inclinations, and other characteristics of the exo-planetary system. 

 # Project status

Project is in its testing phase. 
The demonstrated here version of run-NB-drag.py is tuned to test the functionality of gas drag force. 
As name suggests, it is dealing only with n-body without the statistical mc_coag. It has shown to have a problematic behaviour in extreme cases. 
 
